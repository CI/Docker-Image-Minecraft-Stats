FROM ubuntu:22.04
RUN apt-get update; apt-get install -y wget gpg unzip
RUN wget -O - https://apt.corretto.aws/corretto.key | gpg --dearmor -o /usr/share/keyrings/corretto-keyring.gpg && \
echo "deb [signed-by=/usr/share/keyrings/corretto-keyring.gpg] https://apt.corretto.aws stable main" | tee /etc/apt/sources.list.d/corretto.list
RUN apt-get update; apt-get install -y nginx java-17-amazon-corretto-jdk supervisor
COPY MinecraftStatsCLI.zip /source/MinecraftStatsCLI.zip
COPY MinecraftStatsWeb.zip /source/MinecraftStatsWeb.zip
COPY stats.conf /etc/nginx/conf.d/stats.conf
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
RUN unzip /source/MinecraftStatsCLI.zip -d /opt/mcstatscli/
RUN unzip /source/MinecraftStatsWeb.zip -d /opt/mcstatsweb/
COPY cs.json /opt/mcstatsweb/localization/en.json
CMD ["supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]